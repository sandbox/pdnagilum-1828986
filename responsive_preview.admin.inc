<?php

/**
 * Compiles the settings form to be displayed in admin.
 */
function responsive_preview_settings() {
  $setting_enabled     = variable_get('responsive_preview_enabled', TRUE);
  $setting_resolutions = variable_get('responsive_preview_resolutions', array());

  sort($setting_resolutions);

  $form['responsive_preview_enabled'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Visible'),
    '#description'   => t('Toggle the visibility of the responsive preview resolution changer.'),
    '#default_value' => $setting_enabled,
  );

  $form['responsive_preview_fieldset'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Resolutions'),
    '#description' => t('Configure the different resolutions that will be available.'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  if (!empty($setting_resolutions)) {
    foreach ($setting_resolutions as $idc => $category) {
      $form['responsive_preview_fieldset'][$idc] = array(
        '#type'        => 'fieldset',
        '#title'       => $category['name'],
        '#collapsible' => TRUE,
        '#collapsed'   => FALSE,
      );

      $form['responsive_preview_fieldset'][$idc]['edit'] = array(
        '#type'   => 'markup',
        '#markup' => '<a href="javascript:;" class="responsive-preview-category-edit" data-id="' . $idc . '">' . t('edit') . '</a>' .
                     ' | ' .
                     '<a href="javascript:;" class="responsive-preview-category-delete" data-id="' . $idc . '" onclick="return confirm(\'' . t('Are you sure? This will delete the category as well as every resolution added to it.') . '\');">' . t('delete') . '</a><br />&nbsp;',
      );

      sort($category['data']);

      if (!empty($category['data'])) {
        $form['responsive_preview_fieldset'][$idc]['tblt'] = array(
          '#type'   => 'markup',
          '#prefix' => '<table class="responsive-preview-table">',
          '#suffix' => '</table>',
        );

        $form['responsive_preview_fieldset'][$idc]['tblt']['header'] = array(
          '#type'   => 'markup',
          '#markup' => '<th>' . t('Resolution') . '</th>' .
                       '<th>' . t('Tooltip') . '</th>' .
                       '<th>' . t('Operations') . '</th>',
          '#prefix' => '<tr>',
          '#suffix' => '</tr>',
        );

        foreach ($category['data'] as $idr => $resolution) {
          $form['responsive_preview_fieldset'][$idc]['tblt'][$idr] = array(
            '#type'   => 'markup',
            '#markup' => '<td class="resolution">' . $resolution['width'] . 'x' . $resolution['height'] . '</td>' .
                         '<td class="name">' .       $resolution['name'] . '</td>' .
                         '<td class="tools">' . '<a href="javascript:;" class="responsive-preview-resolution-edit"   data-id="' . $idr . '">' . t('edit') . '</a> | ' .
                                                '<a href="javascript:;" class="responsive-preview-resolition-delete" data-id="' . $idr . '" onclick="confirm(\'' . t('Are you sure?') . '\');">' . t('delete') . '</a></td>',
            '#prefix' => '<tr>',
            '#suffix' => '</tr>',
          );
        }
      }
    }
  }

  $module_path = drupal_get_path('module', 'responsive_preview');

  drupal_add_css($module_path . '/responsive_preview.admin.css', 'external');
  drupal_add_js($module_path . '/responsive_preview.admin.js', 'external');

  return system_settings_form($form);
}
