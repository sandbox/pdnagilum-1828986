var responsive_preview_wrapper = null;
var responsive_preview_wrapper_border = null;
var responsive_preview_wrapper_border_iframe = null;
var responsive_preview_countdown = 20;

jQuery(document).ready(function () {
  if (window == window.top) {
    jQuery.ajax({
      type: 'GET',
      url: 'pages-ajax/responsive-preview/get-categories',
      success: function (categories) {
        var toolbar = jQuery('<div />').addClass('responsive-preview-toolbar');

        for (var idc = 0; idc < categories.length; idc++) {
          var category = jQuery('<div />').addClass('device-group');
          var title    = jQuery('<span />').addClass('device-group-title').text(categories[idc].name + ':');
          var links    = jQuery('<div />').addClass('device-group-links');

          for (var idr = 0; idr < categories[idc]['data'].length; idr++) {
            var link = jQuery('<a />')
              .addClass('device-link-resize')
              .text(categories[idc]['data'][idr]['width'] + 'x' + categories[idc]['data'][idr]['height'])
              .attr('title', categories[idc]['data'][idr]['name'])
              .attr('data-width', categories[idc]['data'][idr]['width'])
              .attr('data-height', categories[idc]['data'][idr]['height'])
              .attr('href', 'javascript:;');

            link.click(function () {
              responsive_preview_resize_iframe(jQuery(this));
            });

            links.append(link);
          }

          category.append(title);
          category.append(links);

          toolbar.append(category);
        }

        jQuery('body').prepend(toolbar);
      }
    });
  }
  else {
    responsive_preview_remove_admin_menu();
  }
});

/**
 * Removes the admin-menu inside the iframe so you get a pure design. Loops
 *   until it finds the admin-menu, or 20 times, then removes it, and adjusts
 *   the body-tags margin.
 */
function responsive_preview_remove_admin_menu() {
  var admin_menu = jQuery('div#admin-menu');
  if (admin_menu.length > 0) {
    admin_menu.remove();
    jQuery('body').attr('style', 'margin-top: 0px !important;');
  }
  else {
    responsive_preview_countdown--;
    if (responsive_preview_countdown > 0) {
      setTimeout("responsive_preview_remove_admin_menu()", 25);
    }
  }
}

/**
 * The click-event for all the resize-links. Fetches the width and height from
 *   the data-fields on the anchor-tag and resizes the iframe-wrapper
 *   accordingly. If the iframe-wrapper doesn't exist, it's created.
 *
 * @param link
 *   The jQuery object of the anchor-tag itself.
 */
function responsive_preview_resize_iframe(link) {
  var width  = parseInt(link.attr('data-width'));
  var height = parseInt(link.attr('data-height'));

  var window_width = parseInt(jQuery(window).width());
  var window_height = parseInt(jQuery(window).height());

  var left = (parseInt(window_width) - width) / 2;

  jQuery('a.device-link-resize').removeClass('device-link-resize-selected');
  link.addClass('device-link-resize-selected');

  if (responsive_preview_wrapper == null) {
    var toolbar = jQuery('div.responsive-preview-toolbar');
    var top  = parseInt(toolbar.position().top) + parseInt(toolbar.height());

    responsive_preview_wrapper = jQuery('<div />').addClass('responsive-preview-wrapper').css({
      'top': top + 6,
      'left': 0,
      'width': window_width,
      'height': (window_height - top)
    });

    jQuery('body').append(responsive_preview_wrapper);

    responsive_preview_wrapper.click(function () {
      responsive_preview_wrapper.animate({
        'opacity': 0
      }, 300, function () {
        jQuery('a.device-link-resize').removeClass('device-link-resize-selected');

        responsive_preview_wrapper_border_iframe.remove();
        responsive_preview_wrapper_border_iframe = null;

        responsive_preview_wrapper_border.remove();
        responsive_preview_wrapper_border = null;

        responsive_preview_wrapper.remove();
        responsive_preview_wrapper = null;
      });
    });

    responsive_preview_wrapper.animate({
      'opacity': 1
    }, 300, function () {
      var border_width = width + 1;
      var border_height = height + 1;

      responsive_preview_wrapper_border = jQuery('<div />').addClass('responsive-preview-border').css({
        'top': 0,
        'left': left,
        'width': border_width,
        'height': border_height
      });

      responsive_preview_wrapper_border_iframe = jQuery('<iframe />').addClass('responsive-preview-iframe').css({
        'width': width,
        'height': height
      });

      responsive_preview_wrapper_border.append(responsive_preview_wrapper_border_iframe);
      responsive_preview_wrapper.append(responsive_preview_wrapper_border);

      responsive_preview_wrapper_border_iframe.get(0).contentWindow.location.href = window.location;

      responsive_preview_wrapper_border.animate({
        'opacity': 1
      }, 300);
    });
  }
  else {
    responsive_preview_wrapper_border.animate({
      'left': left - 1,
      'width': width + 1,
      'height': height + 1
    }, 300);

    responsive_preview_wrapper_border_iframe.animate({
      'width': width,
      'height': height
    }, 300);
  }
}
